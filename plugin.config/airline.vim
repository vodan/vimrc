"==============================================================================
"Airline
"==============================================================================
let g:airline_left_sep="\u25B6"
let g:airline_right_sep="\u25C0"
let g:airline_theme='jellybeans'
let g:airline#extensions#tabline#enabled = 1
