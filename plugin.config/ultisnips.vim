" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
" extend the search directories for multiple snippet categories.
let g:UltiSnipsSnippetDirectories = ["UltiSnips", "privatSnips", "workSnips"]
