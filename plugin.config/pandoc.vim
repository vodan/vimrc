
let g:pandoc#modules#disabled = ["folding"]

" if file under link does not exist create it
let g:pandoc#hypertext#create_if_no_alternates_exists = 1
